# Soong\Data

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-travis]][link-travis]
[![Coverage Status][ico-scrutinizer]][link-scrutinizer]
[![Quality Score][ico-code-quality]][link-code-quality]
[![Total Downloads][ico-downloads]][link-downloads]

Soong\Data provides basic data representations for the Soong framework.

## Install

Via Composer

``` bash
$ composer require soong/data
```

## Usage

``` php

```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.

### Todo


## Security

If you discover any security related issues, please email `soong@virtuoso-performance.com` instead of using the issue tracker.

## Credits

- [Mike Ryan][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/soong/data.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/soong/data/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/soong/data.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/soong/data.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/soong/data.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/soong/data
[link-travis]: https://travis-ci.org/soong/data
[link-scrutinizer]: https://scrutinizer-ci.com/g/soong/data/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/soong/data
[link-downloads]: https://packagist.org/packages/soong/data
[link-author]: https://gitlab.com/mikeryan776
[link-contributors]: ../../contributors
