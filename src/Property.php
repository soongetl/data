<?php
declare(strict_types=1);

namespace Soong\Data;

/**
 * Immutable data property wrapper implementation.
 *
 * @package Soong\Data
 */
class Property implements DataPropertyInterface
{

    /**
     * The value being wrapped.
     *
     * @var mixed
     */
    protected $value;

    /**
     * Store the value.
     *
     * @param mixed $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function getValue()
    {
        return $this->value;
    }
}
