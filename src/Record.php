<?php
declare(strict_types=1);

namespace Soong\Data;

/**
 * Basic implementation of data records as arrays.
 *
 * @todo: Inject DataProperty implementation.
 *
 * @package Soong\Data
 */
class Record implements DataRecordInterface
{

    /**
     * Array of data properties, keyed by property name.
     * @var \Soong\Data\DataPropertyInterface[]
     */
    protected $data = [];

    /**
     * {@inheritdoc}
     */
    public function fromArray(array $data) : void
    {
        foreach ($data as $propertyName => $propertyValue) {
            $this->data[$propertyName] = new Property($propertyValue);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setProperty(string $propertyName, ?DataPropertyInterface $propertyValue) : void
    {
        $this->data[$propertyName] = $propertyValue;
    }

    /**
     * {@inheritdoc}
     */
    public function getProperty(string $propertyName) : ?DataPropertyInterface
    {
        return isset($this->data[$propertyName]) ? $this->data[$propertyName] : null;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray() : array
    {
        $result = [];
        foreach ($this->data as $propertyName => $propertyValue) {
            if (!is_null($propertyValue)) {
                $result[$propertyName] = $propertyValue->getValue();
            }
        }
        return $result;
    }
}
